require ipmimanager
require essioc

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES","10000000")

epicsEnvSet("P", "DTL-030:RFS")

# when set to 0 console will not produce message after ioc init, when set to 1 all messages will be visible
epicsEnvSet("DISP_MSG", 1)

# crate IP address
epicsEnvSet("CRATE_NUM", "1")
epicsEnvSet("MCH_ADDR", "dtl3-rf-mtca-mch.tn.esss.lu.se")
epicsEnvSet("TELNET_PORT", "23")

# 1=enable/0=disable INTERNAL archiver
epicsEnvSet("ARCHIVER", "0")
# INTERNAL archiver size
epicsEnvSet("ARCHIVER_SIZE", 1024)

# Panels compatibility version
epicsEnvSet("PANEL_VER", "2.0.0")

# Special naming mode 0 -> standard naming convention 1 -> special mode
epicsEnvSet("NAME_MODE", 0)

# Deployment path (with substitutions files)
epicsEnvSet("DEPLOYMENT_DIR", "auto-gen-db")

###########################
# connect to specific MCH
############################
epicsEnvSet("MTCA_PREF", "$(P)-MTCA-$(CRATE_NUM)00:")
epicsEnvSet("IOC_PREF", "$(P)-IOC-$(CRATE_NUM)00:")

epicsEnvSet("SLOT1_MODULE", "CPU")
epicsEnvSet("SLOT1_IDX", "01")
epicsEnvSet("SLOT2_MODULE", "EVR")
epicsEnvSet("SLOT2_IDX", "01")
epicsEnvSet("SLOT3_MODULE", "DIG")
epicsEnvSet("SLOT3_IDX", "01")
epicsEnvSet("SLOT4_MODULE", "DIG")
epicsEnvSet("SLOT4_IDX", "02")
epicsEnvSet("SLOT5_MODULE", "DIG")
epicsEnvSet("SLOT5_IDX", "03")
epicsEnvSet("SLOT10_MODULE", "AMC")
epicsEnvSet("SLOT10_IDX", "01")
epicsEnvSet("SLOT19_MODULE", "RTM")
epicsEnvSet("SLOT20_MODULE", "RTM")
epicsEnvSet("SLOT21_MODULE", "RTM")

epicsEnvSet("CHASSIS_CONFIG", "SLOT1_MODULE=$(SLOT1_MODULE)", "SLOT1_IDX=$(SLOT1_IDX)", "SLOT2_MODULE=$(SLOT2_MODULE)", "SLOT2_IDX=$(SLOT2_IDX)", "SLOT3_MODULE=$(SLOT3_MODULE)","SLOT3_IDX=$(SLOT3_IDX)", "SLOT4_MODULE=$(SLOT4_MODULE)", "SLOT4_IDX=$(SLOT4_IDX)", "SLOT5_MODULE=$(SLOT5_MODULE)", "SLOT5_IDX=$(SLOT5_IDX)", "SLOT10_MODULE=$(SLOT10_MODULE)", "SLOT10_IDX=$(SLOT10_IDX)", "SLOT19_MODULE=$(SLOT19_MODULE)", "SLOT20_MODULE=$(SLOT20_MODULE)", "SLOT21_MODULE=$(SLOT21_MODULE)") 

# load db templates for your hardware configuration
iocshLoad("chassis.iocsh")

iocshLoad("$(ipmimanager_DIR)connect.iocsh", "DB_NAME=dynamic, MCH_ADDR=$(MCH_ADDR), ARCHIVER=$(ARCHIVER), ARCHIVER_SIZE=$(ARCHIVER_SIZE), P=$(P), CRATE_NUM=$(CRATE_NUM), TIMEOUT=10, USE_STREAM=, STREAM_PORT=$(TELNET_PORT),CHECK_VER=#,PANEL_VER=$(PANEL_VER),NAME_MODE=$(NAME_MODE),MTCA_PREF=$(MTCA_PREF),IOC_PREF=$(IOC_PREF),USE_EXPERT=,$(CHASSIS_CONFIG),DEPLOYMENT_DIR=$(DEPLOYMENT_DIR)")  # With steam reading links (mch not password protected)

iocInit()

eltc "$(DISP_MSG)"
